
---
# Python Script Flow Control

.footer: Created by Alex M. Schapelle, VaioLabs.IO

---
# Python Script Flow Control

## Python Conditions and If statements

Python supports the usual logical conditions from mathematics:

*  Equals: a == b
*  Not Equals: a != b
*  Less than: a < b
*  Less than or equal to: a <= b
*  Greater than: a > b
*  Greater than or equal to: a >= b


---

# Python Script Flow Control

## Python Conditions and If statements (cont.)


These conditions can be used in several ways, most commonly in "if statements" and loops.
An "if statement" is written by using the `if` keyword. The condition the `if` keyword is checking, is called `expression`. Expression specifies the conditions which are based on `Boolean` expression. When a Boolean expression is evaluated it produces either a value of true or false. If the expression evaluates true the same amount of indented statement(s) following if will be executed. This group of the statement(s) is called a block.

---

# Python Script Flow Control

## Indentation

```py
var=5
     var=5 # this will be error
```
> Python relies on indentation (whitespace at the beginning of a line) to define scope in the code. Other programming languages often use curly-brackets for this purpose.

---

# Python Script Flow Control

## If .. else

`if .. else` statement in python has two blocks: one following the `if` expression and other following the `else` clause.
if the expression evaluates to `true` the same amount of indented statements(s) following if will be executed and if the expression evaluates to false the same amount of indented statements(s) following else will be executed

---
# Practice
- Write a Python script to find those numbers which are divisible by 7 and multiple of 5, between 1500 and 2700 (both included

---
# Python Script Flow Control

## else
Python evaluates each expression (i.e. the condition) one by one and if a true condition is found the statement(s) block under that expression will be executed. If no true condition is found the statement(s) block under else will be executed.

> The `else` keyword catches anything which isn't caught by the preceding conditions.

---

# Python Script Flow Control

## Nested if .. else statement

In general nested if-else statement is used when we want to check more than one conditions, under existing condition. Conditions are executed from top to bottom and check each condition whether it evaluates to true or not. If a true condition is found the statement(s) block associated with the condition executes otherwise it goes to next condition.

```py
if true:
    if true:
        print('True')
```


---
# Define a negative if

If a condition is true the `not` operator is used to reverse the logical state, then logical `not` operator will make it false. 

```py
if not false:
    if true:
        print('True')
```
---

# Practice

- Run in REPL and write the same code in code editor.(try to incorporate all that we have learned)
    - Print "Hello World" if a is greater than b.
    - Print "Hello World" if a is not equal to b.
    - Print "Yes" if a is equal to b, otherwise print "No".
    - Print "1" if a is equal to b, print "2" if a is greater than b, otherwise print "3".
    - Print "Hello" if a is equal to b, and c is equal to d.
    - Print "Hello" if a is equal to b, or if c is equal to d.
    - What is wrong with this: 
    ```sh
    if 5 > 2:
    print("Five is greater than two!")
    ```

