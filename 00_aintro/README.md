# Python Scripting



.footer: Created By Alex M. Schapelle, VAioLabs.io


---
# Who is this for ?

- The name kind of mentions it:
    - System administrators who wish to learn basic and advance python scripting.
- But it also can be useful for:
    - Junior DevOps who wish to gain knowledge of python programming.
    - Junior software developers who have no knowledge in regards version control.

---

# Prerequisites

- *Nix based working computer: Unix, FreeBSD, Well known Linux Distribution.
- Basic knowledge of hardware.
- Basic understanding of CLI: Bash, or alike(Any POSIX should be fine), in some cases PowerShell.
- Minimal understanding of filesystem.
- Some **Programming** or **Scripting** experience with other programming/scripting languages can be useful but __not__ necessary .

---

# Course Topics

- [Basic](../00_basics/README.md)
- [Scripting Intro](../01_scripting_intro/README.md)
- [Conditions](../02_scripting_conditions/README.md)
- [Storing Data](../03_storing_data/README.md)
- [Scripting Loops](../04_scripting_loops/README.md)
- [Functions](../05_functions/README.md)
- [Object Oriented Programming](../06_oop/README.md)

---

# Course Topics (cont.)

- [Exception Handling](../07_working_with_files_and_exceptions/README.md)
- [Working With Files](../07_working_with_files_and_exceptions/README.md)
- [Modules and Libraries](../08_modules_and_libs/README.md)
- [Modules: system](../09_modules/20_system_module/README.md)
- [Modules: parameters](../09_modules/21_scripting_parameters/README.md)
- [Modules: network](../09_modules/22_net_modules/README.md)
- [Modules: database](../09_modules/23_db_modules/README.md)

---
# About Me

<img src="99_misc/.img/me.jpg" alt="drawing" style="float:right;width:180px;">

- Over 12 years of IT industry Experience.
- Fell in love with AS-400 unix system at IDF.
- 5 times tried to finish degree in computer science field
    - Between each semester, I tried to take IT course at various places.
        - Yes, one of them was A+.
        - Yes, one of them was Cisco.
        - Yes, one of them was RedHat course.
        - Yes, one of them was LPIC1 and Shell scripting.
        - No, others i learned alone.
        - No, not maintaining debian packages any more.
---

# About Me (cont.)
- Over 7 years of sysadmin:
    - Shell scripting fanatic
    - Python developer
    - JS admirer
    - Golang fallen
    - Rust fan
- 5 years of working with devops
    - Git supporter
    - Vagrant enthusiast
    - Ansible consultant
    - Container believer
    - K8s user

You can find me on linked-in: [Alex M. Schapelle](https://www.linkedin.com/in/alex-schapelle)

---
# And Now

- Back to course material

