
---
# Python Script Intro


.footer: Created by Alex M. Schapelle, VaioLabs.IO

---
# Namespaces

`Namespaces` are the constructs used for organizing the names assigned to the objects in a program. Essentially, A namespace is a collection of names and the details of the objects referenced by the names.

### What is a built-in namespace in Python?
A built-in namespace contains the names of built-in functions and objects. It is created while starting the python interpreter, exists as long as the interpreter runs, and is destroyed when we close the interpreter. It contains the names of built-in data types, exceptions and functions like `print()` and `input()`.

### What is a global namespace in python?

Global namespaces are defined at the program or module level. It contains the names of objects defined in a module or the main program. A global namespace is created when the program starts and exists until the program is terminated by the python interpreter

---
# Variables

- How to keep data on your memory?  Identifier names
    - Case sensitive:  my_var is not my_VAR
    - Should start with _ or a-z or A-Z
    - Can not be saved words

---
# Variables

- Conventions:
  - `_my_var`: mostly used for `internal` or `private` use, will not be imported in case of use as library or module
  - `__my_vat`: used for OOP, mostly useful with `attributes`, in other cases usable, but not suggested
  - `__my_vax__`: usable but also a syntax used by python for python_system and might create troubles, so do not use

---

# Variables

Other conventions

- Variables: lower-case, words separated with underscore(snake_case)
- Constants: ALL UPPER-CASE, words separated with underscore(snake_case)
- Functions: lower-case, words separated with underscore(snake_case)
- Classes: CapWords, also known as camel-case
- Modules: lower-case, short names, can have underscore, but not required
- Packages: lower-case, short names, preferred not to have underscore

---
# Statements and Comments

### How to remember what you have done.... a year ago?
Essentially we need to write code that is readable. Some time additional explanation for rational is required, thus comments are the best place to start with.

---
# Practice

- Create python script file where you:
  - Define variable for your name with value of your name
  - Define variable of your nickname with value of your nickname
  - Define variable of your pet,(children can be considered as pets) with value of your pets name
- Save the file and run it.

---
# Data types

- Its all about definitions, but cpu still needs to know what you mean.
    - Numbers: integers and floats (complex too)
    - Strings: combined characters
    - Boolean: True/False or 1/0
    - None: null/void/nil or anything that can describe ___
    - There are also data structures that enable us to store more than one value, but we'll cover those later:
        - Lists, Sets, Dictionaries and Tuples

---
# Practice

- Create python script file where you:
    - create variable for holding your name
    - create variable for holding your age
    - create variable for holding your pets name
    - use `print()` function to print them all.
- Save the file and run it.

--- 
# Data types
In python every data type has its own features, and these features are unique to each and every one of the types __only__.
We'll  demo most of the features  but keep in mind that one must alway address documentation to see updates and new features in order to learn.

- The link to python documentation: `https://docs.python.org/3.7`
- In case of absence of internet connection, use `help()` function to get description of specific command in python shell
-  > `[!]` Note: to each new version of python, there might be changes in already existing features yet new features will be added to newer version of python.


---
# Data types
## Integers

Int, or integer, is a whole number, positive or negative, without decimals, of unlimited length.
- `int()` function enables us to cast data into integer


---
# Data types
## Floats

Float, or "floating point number" is a number, positive or negative, containing one or more decimals.
- `float()` function enables us to cast data into floating point number
- Float can also be scientific numbers with an "e" to indicate the power of 10.
  

---
# Data types

## Strings

Strings in python are surrounded by either single quotation marks, or double quotation marks.
'hello' is the same as "hello".
You can display a string literal with the `print()`

- `str()` function enables us to cast data into strings > `iface=str(123) # will be saved as string`
- Assigning a string to a variable is done with the variable name followed by an equal sign and the string
- You can assign a multiline string to a variable by using three double quotes Or three single quotes
- Like many other popular programming languages, strings in Python are arrays of bytes representing unicode characters.


---
# Data types

## Strings (cont.)

- However, Python does not have a character data type, a single character is simply a string with a length of 1.
- To get the length of a string, use the `len()` function.
- To check if a certain phrase or character is present in a string, we can use the keyword `in`.
- To check if a certain phrase or character is NOT present in a string, we can use the keyword `not in`.


---

# Data types

## Boolean
In programming you often need to know if an expression is True or False.
You can evaluate any expression in Python, and get one of two answers, True or False.
When you compare two values, the expression is evaluated and Python returns the Boolean answer:

- bool() function enables us to cast data into boolean > ` bool("abc") # will evaluate as True`
- Almost any value is evaluated to True if it has some sort of content.
- Any string is True, except empty strings.
- Any number is True, except 0.
- Any list, tuple, set, and dictionary are True, except empty ones.
  
There are not many values that evaluate to False, except empty values, such as (), [], {}, "", the number 0, and the value None. And of course the value False evaluates to False.

---
# Data types

# None

Representation of the absence of a value, often called null in other languages

---
# Type conversion

- You can change anything, if you convert it correctly. (mostly)
    - int/float -> string
    - string -> int/float
    - boolean -> int/string
    - int -> boolean
    - string -> boolean
- rational: in future lessons, we'll be using code provided by others and we'll try to incorporate data that code provides in our use cases, so learning how to convert one data type to other on the go is something we should know.

---

# Practice

- Create python script file where you:
    - create variable for holding your spouse (ife/husband) name
            - check and print  the type of that variable.
    - create variable for holding your spouses age
            - convert age variable to type string 
    - create variable to hold your credit card details (if you wish, you can send them over the email to me: Joking....)
        - `print()` them away.
- Save the file and run it.

---
# Operators

- Good! You have types and where to keep em'... but what else we can do with them ?
- We use operators to work with data(data can be considered anything, but mostly variable and data-structures):
    - Math Operators: + - * / ** // %
    - Assignment Operators: = += -= *= /= %=
    - Compare Operators: <> <= => == !=
    - Logical Operators: and or not
    - Membership Operators: is is not
    - Bitwise Operators: & | ^ ~ >> <<

---
# Operators Precedence 

Python has it too, so here is table that describes precedence from highest to lowest

|  Operator                               |   Description |
| ---                                     |   ---         |
| (expressions...),
[expressions...], {key: value...}, {expressions...} | Binding or parenthesized expression, list display, dictionary display, set display |
| x[index], x[index:index], x(arguments...), x.attribute | Subscription, slicing, call, attribute reference | 
| await x | Await expression |
| ** |  Exponentiation  |
| +x, -x, ~x | Positive, negative, bitwise NOT |
| *, @, /, //, % | Multiplication, matrix multiplication, division, floor division, remainder 
| +, - | Addition and subtraction | 
| <<, >> | Shifts |
| & | Bitwise AND |
| ^ | Bitwise XOR |
| \| | Bitwise OR | 
| in, not in, is, is not, <, <=, >, >=, !=, == | Comparisons, including membership tests and identity tests|
| not x | Boolean NOT |
| and | Boolean AND | 
| or | Boolean OR | 
| if – else | Conditional expression|
| lambda |Lambda expression | 
| := | Assignment expression |

---

# Practice

- Create python script file where you:
    - create variable for wifi/eth/network that you are connected to, and put in it that name.
    - create variable for your laptops hostname and save the name in it.
    - concatenate the variable above with new variable named   `my_con`.
    - `print()` the variable
    - add id (or any number) of your connection to my_con variable and re-print it. 


---
# I/O and Import

- Working with OS StdIn and StdOut or StdErr
- Working with files and standard library of python
    - We'll cover this later on.


---
# Slices

- You can return a range of characters by using the slice syntax.
- Specify the start index and the end index, separated by a colon, to return a part of the string.
- By leaving out the __start__ index, the range will start at the first character
- By leaving out the __end__ index, the range will go to the end
- Use negative indexes to start the slice from the end of the string

---
# Practice

- Open REPL and create variable `iface` with value of your default interface
    - print value of the variable
    - present 3rd to 7th strings
    - show 2nd character from the end
    - check of 1st letter is `capital()`

---
# String Format 

Python has a set of built-in methods that you can use on strings.

- upper()
- lower()
- strip() : rstrip() lstrip()
- replace()
- split()
- format()
- is.. : islower() isupper() isdigit()
- find()
- index()

---
# String concatenation

To concatenate, or combine, two strings you can use the + operator.

---
# String escape characters

| Code 	| Result          |
| :---: |  :-----:        |
| \' 	| Single Quote 	  |
| \\ 	| Backslash 	  |
| \n 	| New Line 	  |
| \r 	| Carriage Return |	
| \t 	| Tab 	          |
| \b 	| Backspace 	  |
| \f 	| Form Feed       |	
| \ooo 	| Octal value 	  |
| \xhh 	| Hex value 	  |

---
# Practice

- Create a python script
    - request from user to input some data __twice__
        - on the first input add back slash as part of input
            - print the input immediately
        - on the second input add double quote as part input
            - seek for double quote and print its position
---
# Summary


