# Python Scripting Syllabus

.footer: Created by Alex M. Schapelle, VaioLabs.io

---

# Lesson 1

- History
    - How Python came to be
    - Basic explainer of programming concepts 
- Environment Setup
    - Python install/compile
    - REPL and how to use it <!-- ipython -->
    - Development tools
    - Version control for beginners
- Python Syntax
    - Spaces not tabs
    - Naming conventions
    - Basic program structures
    - Entry point and how to use it
- Data Types
    - Integers
    - Floats
    - Strings
    - Boolean
    - Operators
- Variables
    - Storing data
    - Casting
    - Converting
- Collections
    - List
    - Tuple
    - Set
    - Dictionary
    - Operators
- Conditions
    - conditional boolean
    - conditional structure
    - operators and conditions
- Summary

---

# Lesson 2

- Recap
- Looping
    - Serial loops
    - Conditional loops
    - Operators in Loops <!-- break/continue-->
    - Looping thought data <!-- built-in functions-->
- Functions
    - What is function ? <!-- https://www.youtube.com/watch?v=2v0W83-sxh8&list=PLah0HUih_ZRljCWNZp2N-YBVkgxiJZWEY&index=22-->
    - Variables in function
        - private
        - global
    - Operators and return
    - Conditions and return
    - Loops and return
    - `Main` function  <!-- https://www.youtube.com/watch?v=MNHxfkDb9FY&list=PLah0HUih_ZRljCWNZp2N-YBVkgxiJZWEY&index=23 -->
    - Argument pass to function
    - Dynamic argument passing
    - Key Value argument pass
    - Positional arguments
    - Pack and unpacking positional arguments
    - Doc-strings
    - Built-in functions
    - Magic functions <!-- map filter all any zip -->
- Summary

---

# Lesson 3

- Recap
- Exceptions
    - Use cases for errors <!-- zero division -->
    - handling errors
    - types of errors
- Working with files
- Modules
    - What is a module
    - How to `import` modules
    - Importing without execution
    - Module path
    - Installing modules
    - Virtual environments <!-- pipenv/poetry -->
    - Debugging code with python
- Standard library
    - os
    - sys
    - subprocess
    - getpass
    - math
    - random
    - date
    - sqlite
- Summary

---

# Lesson 4

- Recap
- CSV
- Json
- Xml-to-dict
- Yaml
- Socket
- Http handling with requests
- Telnet connection with telnetlib
- Expecting communications with pexpect
- File transfer with ftplib
- Ssh connections with paramiko
- Summary 

---

# Lesson 5

- Recap
- Ssh connections with netmiko
- Web Services
    - Rest and SOAP
- Web application
    - Flask
- End Project
- Summary
