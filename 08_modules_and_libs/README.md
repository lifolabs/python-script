# Libs

Python uses some terms that you may not be familiar with if you’re coming from a different language. Among these are __scripts__, __modules__, __packages__, and __libraries__.

A __script__ is a Python file that’s intended to be run directly. When you run it, it should do something. This means that scripts will often contain code written outside the scope of any classes or functions.

A __module__ is a Python file that’s intended to be imported into scripts or other modules. It often defines members like classes, functions, and variables intended to be used in other files that import it.

---
# Libs (cont.)

A __package__ is a collection of related modules that work together to provide certain functionality. These modules are contained within a folder and can be imported just like any other modules. This folder will often contain a special __init__ file that tells Python it’s a package, potentially containing more modules nested within subfolders

A __library__ is an umbrella term that loosely means “a bundle of code.” These can have tens or even hundreds of individual modules that can provide a wide range of functionality. Matplotlib is a plotting library. The Python Standard Library contains hundreds of modules for performing common tasks, like sending emails or reading JSON data. What’s special about the Standard Library is that it comes bundled with your installation of Python, so you can use its modules without having to download them from anywhere.

These are not strict definitions. Many people feel these terms are somewhat open to interpretation. Script and module are terms that you may hear used interchangeably.

---

# 3rd Party Libraries

In the remaining part of this course, we’ll be working with various packages that are and also aren’t included with Python by default.Many programming languages offer a package manager that automates the process of installing, upgrading, and removing third-party packages. Python is no exception.
The de facto package manager for Python is called pip. Historically, pip had to be downloaded and installed separately from Python. As of Python 3.4, it’s now included with most distributions of the language.

---

# Working With Modules
A module is a file containing Python code that can be reused in other Python code files.
Technically, every Python file that you’ve created while reading this book is a module, but you haven’t seen how to use code from one module inside another.
There are four main advantages to breaking a program into modules:

- __Simplicity__: Modules are focused on a single problem.
- __Maintainability__: Small files are better than large files.
- __Reusability__: Modules reduce duplicate code.
- __Scoping__: Modules have their own namespaces




# Creating Modules

# Importing One Module Into Another

# Import Statement Variations

# Why Use Namespaces?

# Working With Packages

# Importing Modules From Packages

# Import Statement Variations for Packages

# Guidelines for Importing Packages

# Importing Modules From Subpackages