
---
# Python Script loops

.footer: Created by Alex M. Schapelle, VaioLabs.IO

---
# For Loop

A for loop is used for iterating over a sequence (that is either a list, a tuple, a dictionary, a set, or a string).
This is less like the for keyword in other programming languages, and works more like an iterator method as found in other object-orientated programming languages.
With the for loop we can execute a set of statements, once for each item in a list, tuple, set etc.
```py
number = (1,2,3,4,5,6,7)
for x in number:
  print(x)
```
---
# For Loop (cont.)

Even strings are iterable objects, they contain a sequence of characters:

```py
for x in "cucumber":
  print(x)
```
---
# Practice


- Write a Python script to find those numbers which are divisible by 7 and multiple of 5, in `range` of 1500 and 2700 (both included)
- Write a Python script that accepts a word from the user and reverse it,
- Write a Python script to construct the triangle pattern, using a nested for loop.
```sh
#
##
###
####
```

---
# For Loop (cont.)
With the `break` statement we can stop the loop before it has looped through all the items:

```py
veggies = ["tomato", "cabbage", "cucumber"]
for x in veggies:
  print(x)
  if x == "cucumber":
    break
```

---

# For Loop (cont.)

`continue` operator returns control to the beginning of loop. That is, continue allows to «jump» remaining expressions in loop and go to the next iteration.
```py
veggies = ["tomato", "cabbage", "cucumber"]
for x in veggies:
  if x == "veggies":
    continue
  print(x)
```
---
# For Loop (cont.) 
### Range Function

To loop through a set of code a specified number of times, we can use the range() function,
The `range()` function returns a sequence of numbers, starting from 0 by default, and increments by 1 (by default), and ends at a specified number.
```py
for x in range(6):
  print(x)
```
The `range()` function defaults to 0 as a starting value, however it is possible to specify the starting value by adding a parameter: range(2, 6), which means values from 2 to 6 (but not including 6):
```py
for x in range(2, 6):
  print(x)
```

The `range()` function defaults to increment the sequence by 1, however it is possible to specify the increment value by adding a third parameter: range(2, 30, 3):
```py
 for x in range(2, 30, 3):
  print(x) 
```
---
# For Loop (cont.) 

The `else` keyword in a for loop specifies a block of code to be executed when the loop is finished:

```py
for x in range(6):
  print(x)
else:
  print("Finally finished!")

```
> `[!]`Note: The else block will NOT be executed if the loop is stopped by a break statement.

Break the loop when x is 3, and see what happens with the else block:

```py
for x in range(6):
  if x == 3: break
  print(x)
else:
  print("Finally finished!") 
```

---
# Practice

- write a python script, that accepts list of MAC addresses (e.g your ethernet and wifi )and checks if they are in saved template (provided below). in case found, prints the message notifying you.
- write a python script, that accepts single MAC address and checks in provided list, if it is there, stops with notice.

```sh
template = ['02:42:56:83:f7:3f', 'f8:59:71:1c:7f:19', '02:42:43:e4:33:cf', '54:e1:ad:11:0f:2d']

```
---

# For Loop (cont.) 

A nested loop is a loop inside a loop.
The "inner loop" will be executed one time for each iteration of the "outer loop":

```py
adj = ["red", "yellow", "green"]
veggies = ["tomato", "cabbage", "cucumber"]

for x in adj:
  for y in veggies:
    print(x, y) 
```
---
# For Loop (cont.) 
###  Pass Statement

for loops cannot be empty, but if you for some reason have a for loop with no content, put in the pass statement to avoid getting an error.
```py
for x in [0, 1, 2]:
  pass 
```
---
# Loop Through a List

You can loop through the list items by using a for loop:


Print all items in the list, one by one:
```py
thislist = ["tomato", "cabbage", "cucumber"]
for x in thislist:
  print(x)
```
Learn more about for loops in our Python For Loops Chapter.

---
# Loop Through the Index Numbers

You can also loop through the list items by referring to their index number.

Use the range() and len() functions to create a suitable iterable.


Print all items by referring to their index number:
```py
thislist = ["tomato", "cabbage", "cucumber"]
for i in range(len(thislist)):
  print(thislist[i])
```

The iterable created in the  above is [0, 1, 2].

---

# Practice

- Import `subprocess` library
- Use `getoutput` function to get output of your systems ip interface information
- Loop through output and get interfaces that are up

---

# Looping Using List Comprehension

List Comprehension offers the shortest syntax for looping through lists:

A short hand for loop that will print all items in a list:

```py
thislist = ["tomato", "cabbage", "cucumber"]
[print(x) for x in thislist]
```

---

# List Comprehension

List comprehension offers a shorter syntax when you want to create a new list based on the values of an existing list.

Based on a list of veggies, you want a new list, containing only the veggies with the letter "a" in the name.

Without list comprehension you will have to write a for statement with a conditional test inside:

```py
veggies = ["tomato", "cabbage", "cucumber", "soybean", "wasabi"]
newlist = []

for x in veggies:
  if "a" in x:
    newlist.append(x)

print(newlist)
```
---

With list comprehension you can do all that with only one line of code:
```py

veggies = ["tomato", "cabbage", "cucumber", "soybean", "wasabi"]

newlist = [x for x in veggies if "a" in x]

print(newlist)
```

---
# The Syntax

```py
newlist = [expression for item in iterable if condition == True]
```

The return value is a new list, leaving the old list unchanged.

---
# Condition

The condition is like a filter that only accepts the items that valuate to True.

Only accept items that are not "tomato":

```py
newlist = [x for x in veggies if x != "tomato"]
```

The condition `if x != "tomato"`  will return True for all elements other than "tomato", making the new list contain all veggies except "tomato".

The condition is optional and can be omitted:


With no if statement:
```py
newlist = [x for x in veggies]
Iterable
if x != "tomato"
```
---

# Iterable 

The iterable can be any iterable object, like a list, tuple, set etc.
You can use the range() function to create an iterable:
```py
newlist = [x for x in range(10)]
```

Same , but with a condition:

Accept only numbers lower than 5:
```py

newlist = [x for x in range(10) if x < 5]
```

--
# Expression

The expression is the current item in the iteration, but it is also the outcome, which you can manipulate before it ends up like a list item in the new list:

Set the values in the new list to upper case:
```py
newlist = [x.upper() for x in veggies]
```
---


You can set the outcome to whatever you like:
Set all values in the new list to 'hello':

```py
newlist = ['hello' for x in veggies]
```
---

The expression can also contain conditions, not like a filter, but as a way to manipulate the outcome:

Return "lemongrass" instead of "cabbage":
```py
newlist = [x if x != "cabbage" else "lemongrass" for x in veggies]
```

The expression in the  above says:

"Return the item if it is not cabbage, if it is cabbage return lemongrass".

---

# Loop Through a Tuple

You can loop through the tuple items by using a for loop.

Iterate through the items and print the values:
```py
thistuple = ("tomato", "cabbage", "cucumber")
for x in thistuple:
  print(x)
```
---
# Loop Through the Index Numbers

You can also loop through the tuple items by referring to their index number.
Use the range() and len() functions to create a suitable iterable.

Print all items by referring to their index number:
```py
thistuple = ("tomato", "cabbage", "cucumber")
for i in range(len(thistuple)):
  print(thistuple[i])
```

---
# Practice

- Import `ipaddress` library
- Create variable that uses `ip_address` method with network segment
- Loop through the items in the variables `host()` data.

---

# While Loop

With the while loop we can execute a set of statements as long as a condition is true.

```py
i = 1
while i < 8:
  print(i)
  i += 1
```
> `[!]`Note: remember to increment i, or else the loop will continue forever.

The while loop requires relevant variables to be ready, in this  we need to define an indexing variable, i, which we set to 1.
---
# While Loop (cont.)

With the break statement we can stop the loop even if the while condition is true:
Exit the loop when i is 3:
```py
i = 1
while i < 8:
  print(i)
  if i == 3:
    break
  i += 1 
```

---
# While Loop (cont.)

With the continue statement we can stop the current iteration, and continue with the next:

Continue to the next iteration if i is 3:
```py
i = 0
while i < 6:
  i += 1
  if i == 3:
    continue
  print(i)
```
---

# Practice

---

# While Loop (cont.)

With the else statement we can run a block of code once when the condition no longer is true:
Print a message once the condition is false:
```py
i = 1
while i < 8:
  print(i)
  i += 1
else:
  print("i is no longer less than 8")
```
---

# Using a While Loop with Lists

You can loop through the list items by using a while loop.
Use the len() function to determine the length of the list, then start at 0 and loop your way through the list items by referring to their indexes.
Remember to increase the index by 1 after each iteration.


Print all items, using a while loop to go through all the index numbers
```py
thislist = ["tomato", "cabbage", "cucumber"]
i = 0
while i < len(thislist):
  print(thislist[i])
  i = i + 1
```

---
# Practice

---

# Using a While Loop with Tuples

You can loop through the list items by using a while loop.
Use the len() function to determine the length of the tuple, then start at 0 and loop your way through the tuple items by referring to their indexes.
Remember to increase the index by 1 after each iteration.

Print all items, using a while loop to go through all the index numbers:
```py
thistuple = ("tomato", "cabbage", "cucumber")
i = 0
while i < len(thistuple):
  print(thistuple[i])
  i = i + 1 
```
---

# Practice

- Import `sys` library
- request from user provide your with parameters with `argv()` function/method.
- while the amount of elements will not be 10, continue requesting from user to provide parameters
- count the amount of parameters provided and let the user know how many more are left
