#!/usr/bin/env bash 
#set -x
#########################################
#created by Silent-Mobius
#purpose: build script for docker class
#verion: 0.4.12
#########################################

. /etc/os-release

PROJECT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

build_dir_array=($(ls |grep -vE '99_*|README.md|TODO.md|build.sh'))


main(){
    dependency_check
    while getopts "bch" opt
    do
        case $opt in
            b)  seek_all_md_files
                convert_data
                ;;
            c) clean_up
                ;;
            h) _help
                ;;
            *) _help
                ;;
        esac
    done

}

_help() {
    echo '[?] Incorrect use'
    echo "[?] Please use $0 \"-b\" for build and \"-c\" for clean up"
    echo "[?] example: $0 -c"
}

clean_up() {
    echo '[+] cleaning up the previous builds'
    if [ -e $PROJECT/build.md ];then
      rm -rf $PROJECT/build.md
    fi

    if [ -e $PROJECT/presentation.html ];then
        rm -rf $PROJECT/presentation.html
    fi
    
    if [ -e $PROJECT/presentation.pdf ];then
        rm -rf $PROJECT/presentation.pdf
    fi
    find . -name presentation.html -exec rm {} \;
    echo '---------------------------------'
    echo '[+] Cleanup ended successfully   '
    echo '---------------------------------'
}

seek_all_md_files() {
    clean_up
    echo '[+] Building ....'
    find "${build_dir_array[@]}" -name '*.md'|sort|xargs cat > build.md 2> /dev/null

    echo '---------------------------------'
    echo '[+] Generate ended successfully  '
    echo '---------------------------------'
}

check_installer(){
    if [[ $ID == "ubuntu" ]] || [[ $ID == "debian" ]];then
        echo "apt-get"
    elif [[ $ID == "fedora" ]] || [[ $ID == "centos" ]] || [[ $ID == "redhat" ]];then 
        echo "yum"
    else
        echo "[!] Not Supported Distro"
        exit 1
    fi
}

dependency_check(){
    if [ -x /usr/bin/landslide ];then
        _builder=/usr/bin/landslide
        export _builder
        return 0
    elif [ -x /usr/bin/darkslide ];then
        _builder=/usr/bin/darkslide
        export _builder
        return 0
    else
        echo '[!] Dependcy missing: please install darkslide or python-landslide or python3-landslide'
        echo '[*] trying to fix'
            _installer=$(check_installer)
            sudo $_installer install -y python-landslide darkslide python3-landslide
    fi
}
build_file(){
    params=$1
    if [[ ${#_builder} == 0 ]];then
        echo "Builder Issue: Can not find build tool. Please open Bug"
        exit 1
    else
        $_builder -v  -t $PROJECT/99_misc/.simple/ -x $params build.md
    fi
}

convert_data() {

        if [[ $ID == "ubuntu" ]];then
            build_file fenced_code,codehilite,extra,toc,smarty,sane_lists,meta,tables
        elif [[ $ID == "debian" ]];then
            build_file fenced_code,codehilite,extra,toc,smarty,sane_lists,meta,md_in_html,tables
        else
            build_file fenced_code,codehilite,extra,toc,smarty,sane_lists,tables
        fi
}



#######
# Main
#######
main "$@"
