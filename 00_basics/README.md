
---
# Python : Scripting Initials


.footer: Created by Alex M. Schapelle, VaioLabs.IO

---
# Before Scripting Initials
## Hardware -> CPU/RAM/HDD
Before we dive into python or scripting, some basics needs to provided:
For That we need to know about hardware components, their role in big picture and how we utilize it all:

- CPU: Cental Processing Unit
- RAM: Random Access Memory
- HDD/SSD: Hard Drive Disk/Solid State Drive

---
# Before Scripting Initials 

## RAM -> Data Management -> Programming
Most of programs in RAM. When we run a program, it allocates memory in RAM (clears space for itself to execute), and starts to implement the lines of code provided by developer(programmer). In order to process compute logical or mathematical information, it schedules access to CPU. After computing data it goes to HDD/SSD in order to save permanent data that it has computed.

---
# Before Scripting Initials

## Operational System(OS)
OS, is only program that has access to everything on our hardware. Today's software is mainly based on OS to get access to CPU,RAM and HDD. Although there are computer systems that are not build in the same way, and logic to access CPU,RAM and HDD/SSD can be different. for example:

- Routers
- Switches
- SoC
- RTos
- and so on.

---
# Before Scripting Initials

## Software Development

Software development is the process of conceiving, specifying, designing, programming, documenting, testing, and bug fixing involved in creating and maintaining applications, frameworks, or other software components. Software development involves writing and maintaining the source code, but in a broader sense, it includes all processes from the conception of the desired software through to the final manifestation of the software, typically in a planned and structured process

---
# Before Scripting Initials

## Compile and RunTime

- A compile time (or compile-time) describes the time window during which a computer program is compiled. The term is used as an adjective to describe concepts related to the context of program compilation, as opposed to concepts related to the context of program execution (runtime)
- A runtime system or runtime environment is a sub-system that exists both in the computer where a program is created, as well as in the computers where the program is intended to be run. The name comes from the compile time and runtime division from compiled languages, which similarly distinguishes the computer processes involved in the creation of a program (compilation) and its execution in the target machine (the run time).

---

# Before Scripting Initials

## Assembly, C, C++ or C# 

The difference between compile time and run time is an example of what pointy-headed theorists call the phase distinction. It is one of the hardest concepts to learn, especially for people without much background in programming languages. To approach this problem, I find it helpful to ask

- What environment does the program satisfy?
- What can go wrong in this phase?
- If the phase succeeds, what are the post conditions (what do we know)?
- What are the inputs and outputs, if any?

---
# Before Scripting Initials
## Compile Time Environment 

The program need not satisfy any environment. In fact, it needn't be a well-formed program at all. You could feed this HTML to the compiler and watch it spill it out with errors...

- What can go wrong at compile time:
    - Syntax errors
    - Typechecking errors
    - (Rarely) compiler crashes

---
# Before Scripting Initials
## Compile Time Environment

- If the compiler succeeds, what do we know?
    - The program was well formed---a meaningful program in whatever language.
    - It's possible to start running the program. (The program might fail immediately, but at least we can try.)
- What are the inputs and outputs?
    - Input was the program being compiled, plus any header files, interfaces, libraries, or other voodoo that it needed to import in order to get compiled.
    - Output is hopefully assembly code or relocatable object code or even an executable program. Or if something goes wrong, output is a bunch of error messages.

---

# Before Scripting Initials

## REPL - Read Evaluate Print Loop
A read–eval–print loop (REPL), also termed an interactive toplevel or language shell, is a simple interactive computer programming environment that takes single user inputs, executes them, and returns the result to the user; a program written in a REPL environment is executed piecewise

---

# Before Scripting Initials
##  JavaScript, Ruby, Python and many more 

We know nothing about the program's environments:

- They are whatever the programmer put in. Run-time environment are rarely enforced by the compiler alone; it needs help from the programmer.
- What can go wrong are run-time errors:
    - Division by zero
    - Dereferencing a null pointer
    - Running out of memory

---
# Before Scripting Initials
##  JavaScript, Ruby, Python and many more 
- Also there can be errors that are detected by the program itself:
    - Trying to open a file that isn't there
    - Trying find a web page and discovering that an alleged URL is not well formed
- If run-time succeeds, the program finishes (or keeps going) without crashing.
    - Inputs and outputs are entirely up to the programmer. Files, windows on the screen, network packets, jobs sent to the printer, you name it. If the program launches missiles, that's an output, and it happens only at run time :-)



---

# Before Scripting Initials
## Keywords and Identifiers

*_Keywords in Python_*

|False |	class | finally | is     | return   |
|----- | ------   | ------- | ----   | ----     |
| None 	|continue |	for 	| lambda |	try     |
| True 	| def |	from |	nonlocal     |	while   |
| and 	| del |	global | 	not      |	with    |
| as    |	elif | 	if |	or       |	yield   |
| assert |	else |	import           |	pass    |	 
| break |	except | in |	raise    | is       |

